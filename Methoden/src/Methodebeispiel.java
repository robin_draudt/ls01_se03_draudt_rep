
public class Methodebeispiel {

	public static void main(String[] args) {
		
		/*sayHello();
		sayHello("Max");
		add(3, 2);
		addDouble(3.5,7.7);
		summe(3,6,7);
		berechnePreis(1, 10.5, "Monitor");*/
		int ergebnis;
		ergebnis=add(2,3);
		System.out.println(ergebnis);
	}
	
	public static void sayHello() {
		System.out.println("hello...");
	}
	
	public static void sayHello(String name) {
		System.out.println("Hello "+name);
	}
	
	//public static void add(int zahl1, int zahl2) {
	//	System.out.println(zahl1 +" + "+ zahl2+" = "+zahl1+zahl2);
	//}
	
	public static void addDouble(double a, double b) {
		double ergebnis = a+b;
		System.out.println("Zahl1: "+a+"\nZahl2: "+b+"\nErgebnis: "+ergebnis+"\n");
	}
	
	public static void summe(int a, int b, int c) {
		int ergebnis=a+b+c;
		System.out.println(a+" + "+b+" + "+c+" = "+ergebnis);
	}
	
	public static void berechnePreis(int a, double b, String s) {
		double preis = a*b;
		if(a == 1) {
			
			System.out.printf("%d %s kostet %.2f Euro", a, s, preis);
			
		}
		else {
			
			System.out.printf("%d %se kosten %.2f Euro", a, s, preis);
		}
	}
	
	public static int add(int zahl1, int zahl2){
		return zahl1+zahl2;
	}
}