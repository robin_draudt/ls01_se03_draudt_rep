
public class ArrayHelper {

	public static String convertArrayToString(int[] zahlen) {
		String s ="";
		for(int i=0; i < zahlen.length; i++) {
			if(i == zahlen.length-1) {
				s = s + zahlen[i];
			}
			else {
			s = s + zahlen[i] + ", ";
			}
		}
		return s;
	}
	
	public static int[] umdrehen(int[] zahlen) {
		int temp;
		for (int i = 0; i < zahlen.length/2; i++)
		{
			temp = zahlen[i];
			zahlen[i] = zahlen[(zahlen.length - (1+i))];
			zahlen[(zahlen.length - (1+i))] = temp;
			
		}
		return zahlen;
	}
	
	public static int[] umdrehenInNeuemArray(int[] zahlen){
		
		int zahlenNeu[] = new int[zahlen.length];
		
		for(int i=0; i < zahlen.length; i++) {
			zahlenNeu[i] = zahlen[zahlen.length-1-i];
		}
		return zahlenNeu;
	}
	
	
	public static void main(String[] args) {
		
		int arr[] = new int[] {1,2,3,4,5,6,7,8,8990,253657,65,4523};
		
		System.out.println(convertArrayToString(umdrehenInNeuemArray(arr)));
	}

}
