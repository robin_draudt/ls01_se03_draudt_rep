
public class Temperaturtabelle {

	public static void Temp(int anzahl){
		
		double[][] Temperatur = new double[anzahl][2];
		
		for(int i=0; i < anzahl; i++) {
			if(i==0) {
				Temperatur[0][0] = 0.0;
			}else {
				Temperatur[i][0] = Temperatur[i-1][0] + 10.0;
			}
			Temperatur[i][1] = (5.0/9.0)*(Temperatur[i][0]-32.0);
		}
		for(int i=0; i < anzahl; i++) {
			System.out.printf("%5.0f%2s%4s%7.2f%2s%n",Temperatur[i][0], "�F",  "-->", Temperatur[i][1], "�C");
		}
	}
	
	public static void main(String[] args) {
		Temp(100);
	}

}
