public class Main {

    public static void main(String[] args) {

    	//------------Raumschiff erstellen---------------
        Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh�ta");
        Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
        Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni�Var");
        
        //------------Ladung erstellen-------------
        Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung l2 = new Ladung("Borg-Schrott", 5);
        Ladung l3 = new Ladung("Rote Materie", 2);
        Ladung l4 = new Ladung("Forschungssonde", 35);
        Ladung l5 = new Ladung("Bat�leth Klingonen Schwert", 200);
        Ladung l6 = new Ladung("Plasma-Waffe", 50);
        Ladung l7 = new Ladung("Photonentorpedo", 3);

        //----------Ladungen zuweisen-----------
        vulkanier.addLadung(l7);
        vulkanier.addLadung(l4);
        
        romulaner.addLadung(l2);
        romulaner.addLadung(l3);
        romulaner.addLadung(l6);
        
        klingonen.addLadung(l1);
        klingonen.addLadung(l5);
        
        //----------Testbereich-----------
        
        klingonen.photonentorpedoSchiessen(romulaner);
        romulaner.phaserkanoneSchiessen(klingonen);
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch\n");
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        vulkanier.reparaturDurchfuehren(true, true, true, 5);
        vulkanier.photonentorpedosLaden(3);
        vulkanier.ladungsverzeichnisAufraeumen();
        klingonen.photonentorpedoSchiessen(romulaner);
        klingonen.photonentorpedoSchiessen(romulaner);
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        romulaner.zustandRaumschiff();
        romulaner.ladungsverzeichnisAusgeben();
        vulkanier.zustandRaumschiff();
        vulkanier.ladungsverzeichnisAusgeben();

    	System.out.println("Logbucheintr�ge des BroadcastKommunikators:");
    	System.out.println("---------------------------------------------");
        for(int i=0; i < Raumschiff.eintraegeLogbuchZurueckgeben().size(); i++) {

        	System.out.println(Raumschiff.eintraegeLogbuchZurueckgeben().get(i));
        }
        System.out.println("---------------------------------------------");
    }
}