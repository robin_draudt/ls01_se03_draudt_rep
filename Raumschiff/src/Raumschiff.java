import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
	//--------Attribute---------
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private  static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//---------Kontruktoren-----------
	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffsname = "Unbekannt";
	}
	public Raumschiff(
			int photonentorpedoAnzahl,
			int energieversorgungInProzent,
			int zustandSchildeInProzent,
			int zustandHuelleInProzent,
			int zustandLebenserhaltungssystemeInProzent,
			int anzahlDroiden,
			String schiffsname) {
		
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.androidenAnzahl = anzahlDroiden;
		this.schiffsname = schiffsname;
		
	}
	
	
	//-------------Getter und Setter--------------
	public int getPhotonentorpedoAnzahl(){return this.photonentorpedoAnzahl;}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;}
	
	public int getEnergieversorgungInProzent() {return this.energieversorgungInProzent;}
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu) {this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;}
	
	public int getSchildeInProzent() {return this.schildeInProzent;}
	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {this.schildeInProzent = zustandSchildeInProzentNeu;}
	
	public int getHuelleInProzent() {return this.huelleInProzent;}
	public void setHuelleInProzent( int zustandHuelleInProzentNeu) {this.huelleInProzent = zustandHuelleInProzentNeu;}
	
	public int getLebenserhaltungssystemeInProzent() {return this.lebenserhaltungssystemeInProzent;}
	public void setLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzentNeu) {this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzentNeu;}
	
	public int getAndroidenAnzahl() {return this.androidenAnzahl;}
	public void setAndroidenAnzahl(int androidenAnzahl) {this.androidenAnzahl = androidenAnzahl;}
	
	public String getSchiffsname() {return this.schiffsname;}
	public void setSchiffsname(String schiffsname) {this.schiffsname = schiffsname;}
	
	
	//---------- Weitere Methoden------------
	public void addLadung(Ladung neueLadung) {  //Neue Ladung wird hinzugefügt
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) { //Photonentorpedo wird auf Raumschiff r geschossen
		
		if (this.photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-\n "+ this.schiffsname + " hat keine Torpedos mehr geladen!\n");
		}
		else {
			this.photonentorpedoAnzahl -= 1;
			nachrichtAnAlle(this.schiffsname+" hat einen Photonentorpedo auf " + r.schiffsname + " abgeschossen!\n");
			treffer(r);
		}
	}
	
	public void phaserkanoneSchiessen(Raumschiff r) { //Phaser wird auf Raumschiff r geschossen
		
		if (this.energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-\n" + this.schiffsname + " hat weniger als 50% Energie!\n");
		}
		else {
			this.energieversorgungInProzent -= 50;
			nachrichtAnAlle(this.schiffsname+ " hat seine Phaserkanone auf " + r.schiffsname + " abgeschossen\n");
			treffer(r);
		}
	}
	
	private void treffer(Raumschiff r) {			//Treffer wird verzeichnet
		System.out.println(r.getSchiffsname()+" wurde getroffen!\n");
		r.setSchildeInProzent(r.getSchildeInProzent()-50);
		if(r.getSchildeInProzent() < 0) {  					//Falls Schilde < 0 --> Schilde = 0
			r.setSchildeInProzent(0);
		}
		if(r.getSchildeInProzent() == 0) {
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent()-50);
			if(r.getEnergieversorgungInProzent() < 0) {		//Falls Energie < 0 --> Energie = 0
				r.setEnergieversorgungInProzent(0);
			}
			r.setHuelleInProzent(r.getHuelleInProzent()-50);
			if(r.getHuelleInProzent() < 0) { 				//Falls Hülle < 0 --> Hülle = 0
				r.setHuelleInProzent(0);
			}
		}
		if(r.getHuelleInProzent() == 0) {
			r.setLebenserhaltungssystemeInProzent(0);
			r.nachrichtAnAlle("Lebenserhaltungssysteme wurden vernichtet!\n");
		}
	}
	
	public void nachrichtAnAlle(String message) { //Eine Nachricht an Alle
		
		Raumschiff.broadcastKommunikator.add(message);
		System.out.println(message);
		
	}
	
	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {			//Broadcastcommunicator wird als ArrayList zurück gegeben
		return Raumschiff.broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		boolean torpedoVorhanden= false;
		int lagerOrt = 0;
		for(int i=0; i < this.ladungsverzeichnis.size(); i++) {
			if(this.ladungsverzeichnis.get(i).getBezeichnung() == "Photonentorpedo") {
				torpedoVorhanden = true;
				lagerOrt = i;
				break;
			}
		}
		if(torpedoVorhanden == false) {
			System.out.print("Keine Photonentorpedos gefunden!\n");
			nachrichtAnAlle("-=*Click*=-");
		}else {
			if(this.ladungsverzeichnis.get(lagerOrt).getMenge() < anzahlTorpedos) {				// Falls die angegebene Zahl größer als die vorhandene Zahl der Torpedos ist
				this.photonentorpedoAnzahl += this.ladungsverzeichnis.get(lagerOrt).getMenge();
				System.out.println(this.schiffsname+" hat "+this.ladungsverzeichnis.get(lagerOrt).getMenge()+" Photonentorpedo(s) eingesetzt\n");
				this.ladungsverzeichnis.get(lagerOrt).setMenge(0);
				
			}
			else {
				this.photonentorpedoAnzahl += anzahlTorpedos;
				this.ladungsverzeichnis.get(lagerOrt).setMenge(this.ladungsverzeichnis.get(lagerOrt).getMenge()- anzahlTorpedos);
				System.out.println(this.schiffsname+" hat "+anzahlTorpedos+" Photonentorpedo(s) eingesetzt\n");
			}
		}
	}
	
	public void reparaturDurchfuehren(
							boolean schutzschilde,
							boolean energieversorgung,
							boolean schiffshuelle,
							int anzahlDroiden) {
		
		int anzahlSchiffsstrukturen = 0;
		if(schutzschilde == true) {
			anzahlSchiffsstrukturen += 1;
		}
		if(energieversorgung == true) {
			anzahlSchiffsstrukturen += 1;
		}
		if(schiffshuelle == true) {
			anzahlSchiffsstrukturen += 1;
		}
		
		int tatsächlicheAnzahlDroiden=0;
		
		if(this.androidenAnzahl < anzahlDroiden) { //Falls die angegebene Zahl der Droiden höher ist, als die tatsächliche Anzahl der Droiden
			tatsächlicheAnzahlDroiden = this.androidenAnzahl;
		}
		else {
			tatsächlicheAnzahlDroiden = anzahlDroiden;
		}
		
		Random reperaturBonus = new Random();		
		int zufallsZahl = 1 + reperaturBonus.nextInt(99); //Zufallszahl zwischen 1 und 100 würfeln
		int berechnung = zufallsZahl*tatsächlicheAnzahlDroiden/anzahlSchiffsstrukturen;
		this.androidenAnzahl = this.androidenAnzahl - tatsächlicheAnzahlDroiden;  //Anzahl der benutzten Droiden wird von der bisherigen Menge abgezogen
		
		System.out.println(tatsächlicheAnzahlDroiden + " Droiden haben das Schiff " + this.schiffsname + " repariert!");
		System.out.println("--------------------------------------------");
		
		if(schutzschilde == true) {
			if(this.schildeInProzent + berechnung > 100) {				//Falls die Reparatur über 100 geht --> Schilde = 100
				System.out.println("Schilde wurden um "+(100-this.schildeInProzent)+"% repariert!");
				this.schildeInProzent = 100;
			}else {
				this.schildeInProzent = this.schildeInProzent + berechnung;
				System.out.println("Schilde wurden um "+berechnung+"% repariert!");
			}

		}
		if(energieversorgung == true) {
			if(this.energieversorgungInProzent + berechnung > 100) {    //Falls die Reparatur über 100 geht --> Energie = 100
				System.out.println("Energieversorgung wurde um "+(100-this.energieversorgungInProzent)+"% repariert!");
				this.energieversorgungInProzent = 100;
			}else {
				this.energieversorgungInProzent = this.energieversorgungInProzent + berechnung;
				System.out.println("Energieversorgung wurde um "+berechnung+"% repariert!");
			}
		}
		if(schiffshuelle == true) {
			if(this.huelleInProzent + berechnung > 100) {       //Falls die Reparatur über 100 geht --> Hülle = 100
				System.out.println("Hülle wurde um "+(100 - this.huelleInProzent)+"% repariert!");
				this.huelleInProzent = 100;
			}else {
				this.huelleInProzent = this.huelleInProzent + berechnung;
				System.out.println("Hülle wurde um "+berechnung+"% repariert!");
			}
		}
		
		System.out.println("--------------------------------------------\n");
		
	}
	
	public void zustandRaumschiff() {
		
		System.out.println("Zustand von " + this.schiffsname+":");
		System.out.println("-----------------------------");
		System.out.printf("%-24s%-2s%3d%n","Photonentorpedos", ":",this.photonentorpedoAnzahl);
		System.out.printf("%-24s%-2s%3d%n","Energie", ":",this.energieversorgungInProzent);
		System.out.printf("%-24s%-2s%3d%n","Schilde", ":",this.schildeInProzent);
		System.out.printf("%-24s%-2s%3d%n","Hülle", ":",this.huelleInProzent);
		System.out.printf("%-24s%-2s%3d%n","Lebenserhaltungssysteme", ":",this.lebenserhaltungssystemeInProzent);
		System.out.printf("%-24s%-2s%3d%n","Androiden", ":",this.androidenAnzahl);
		System.out.println("-----------------------------\n");
	}
	
	public void ladungsverzeichnisAusgeben() {

		System.out.println("Ladungsverzeichnis von "+this.schiffsname+":");
		System.out.println("------------------------------------------------------");
		for(int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			System.out.println(this.ladungsverzeichnis.get(i));
		}
		System.out.println("------------------------------------------------------\n");
	}
	
	public void ladungsverzeichnisAufraeumen() {
		
		for(int i=0; i < ladungsverzeichnis.size(); i++) {
			if(ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}
}
