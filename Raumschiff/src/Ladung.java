
public class Ladung {
	
	private String bezeichnung;
	private int menge;
	
	public Ladung() {
		
	}
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	public void setBezeichnung(String name) {
		this.bezeichnung = name;
	}
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	public int getMenge() {
		return this.menge;
	}
	public String toString() {
		return String.format("%s%-27s%s%s%3d%s","(Bezeichnung: ", this.bezeichnung, ",", " Menge: ", this.menge, ")");
	}
	
}
