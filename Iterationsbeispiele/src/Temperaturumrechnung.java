import java.util.*;
public class Temperaturumrechnung {

	public static void main(String[] args) {
		System.out.println("Bitte den Startwert in Celsius eingeben: ");
		Scanner s = new Scanner(System.in);
		double startwert = s.nextInt();
		System.out.println("Bitte den Endwertwert in Celsius eingeben: ");
		double endwert = s.nextInt();
		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben: ");
		double schrittweite = s.nextInt();
		
		double zaehler = startwert;
		while(zaehler <= endwert) {
			double  fahrenheit = 32 + (zaehler*1.8);
			System.out.printf("%5.2f�C%10.2f�F%n", zaehler, fahrenheit);
			zaehler = zaehler + schrittweite;
		}
	}
}
