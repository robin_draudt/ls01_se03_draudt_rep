import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		System.out.printf("Bitte geben Sie eine Zahl ein");
		Scanner s = new Scanner(System.in);
		String n = s.next();
		int zaehler = 0;
		int ergebnis = 0;
		while(n.length() > zaehler) {
			ergebnis = ergebnis + Integer.parseInt(String.valueOf(n.charAt(zaehler)));
			zaehler++;
		}
		System.out.println(ergebnis);
	}
}

