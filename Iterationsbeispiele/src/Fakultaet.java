import java.util.Scanner;

public class Fakultaet {

	public static void main(String[] args) {
		System.out.printf("Bitte geben Sie eine Zahl ein");
		Scanner s = new Scanner(System.in);
		int  n = s.nextInt();
		while(n>=20) {
			System.out.println("Zahl ist zu gro�, bitte w�hlen Sie eine Zahl kleiner 20");
			n = s.nextInt();
		}
		int zaehler = 1;
		int ergebnis = 1;
		while(zaehler <= n) {
			ergebnis = ergebnis*zaehler;
			zaehler++;
		}
		System.out.println(ergebnis);
	}
}