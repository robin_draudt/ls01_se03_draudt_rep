public class Wettlauf {

	public static void main(String[] args) {
		System.out.printf("%5s%10s%10s%n","Sekunde", "L�ufer 1", "L�ufer 2");
		System.out.println("----------------------------");
		double ziel = 1000;
		double geschwA = 9.5;
		double geschwB = 7.0;
		double distanzA = 0;
		double distanzB = 250;
		int sekunde = 0;
		while(distanzA <= ziel && distanzB <= ziel) {
			sekunde++;
			distanzA = distanzA + geschwA;
			distanzB = distanzB + geschwB;
			System.out.printf("%3ds  |%7.1fm  |%7.1fm%n", sekunde, distanzA, distanzB);
		}
		
	}

}
