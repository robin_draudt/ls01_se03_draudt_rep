import java.util.Scanner;

public class Iterationsbeispiele {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Geben sie bitte n ein");
		
		int n = s.nextInt();
		int zaehler = 1;
		int ergebnis = 0;
		
		while(n >= zaehler) {
			ergebnis = ergebnis + zaehler;
			zaehler++;
			System.out.println(ergebnis);
		}
		System.out.println("Ergebnis = "+ergebnis);
	}

}
