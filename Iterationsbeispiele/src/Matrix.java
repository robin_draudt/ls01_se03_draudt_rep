import java.util.*;
public class Matrix {
	
	public static void main(String[] args) {								
		String[][] matrix = new String[10][10];								//Matrix wird erstellt
		String empty = "";
		for(int i=0; i<10; i++) {											//Matrix wird bef�llt
			for(int x=0; x<10; x++) {
				if(i == 0) {
					matrix[i][x] = empty+ x;
				}
				else {
					matrix[i][x] = empty+i+x;
				}
			}
		}
		System.out.println(Arrays.deepToString(matrix).replace("], ", "]\n").replace("[[", "[").replace("]]", "]")); // Matrix wird ausgegeben
		System.out.printf("%n%nBitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		Scanner s = new Scanner(System.in);
		int eingabe = s.nextInt();
		while(eingabe < 2 || eingabe > 9) {
			System.out.printf("%nDie Zahl liegt nicht in dem angegebenen Bereich!%nBitte geben Sie eine andere Zahl ein:");
			eingabe = s.nextInt();
		}
		for(int i=0; i<10; i++) {
			for(int x=0; x<10; x++) {
				if(i == 0) {															// Falls nur eine Zahl
					
					if(Integer.parseInt(matrix[i][x])== eingabe) {						//Buchstabe = Eingabe
						matrix[i][x] = " *";
						continue;
					}
					if(matrix[i][x] != "0") {
						if(Integer.parseInt(matrix[i][x]) % eingabe == 0) {					//Durch Eingabe ohne Rest teilbar
							matrix[i][x] = " *";
							continue;
						}
					}
				}
				else{																	// Falls mehr als eine Zahl
					if(eingabe == Integer.parseInt(matrix[i][x])) {
						matrix[i][x] = " *";
						continue;
					}
					if(eingabe == Integer.parseInt(String.valueOf(matrix[i][x].charAt(0)))) {		//Buchstabe 1 = Eingabe
						matrix[i][x] = " *";
						continue;
					}
					if(eingabe == Integer.parseInt(String.valueOf(matrix[i][x].charAt(1)))){		//Buchstabe 2 = Eingabe
						matrix[i][x] = " *";
						continue;
					}
					if(Integer.parseInt(matrix[i][x]) % eingabe == 0) {								// Durch Eingabe ohne Rest teilbar
						matrix[i][x] = " *";
						continue;
					}
					if(Integer.parseInt(String.valueOf(matrix[i][x].charAt(0))) + Integer.parseInt(String.valueOf(matrix[i][x].charAt(1))) == eingabe) {		//Quersumme = Eingabe
						matrix[i][x] = " *";
					}
				}
			}
		}
		System.out.println(Arrays.deepToString(matrix).replace("], ", "]\n").replace("[[", "[").replace("]]", "]")); // Matrix wird ausgegeben
	}
}
