
public class Buch {

	private String titel;
	private double preis;
	
	public Buch(String titel, double preis) {
		this.titel = titel;
		this.preis = preis;
	}
	
	public String getTitel() {return this.titel;}
	public void setTitel(String titel) {this.titel = titel;}
	public double getPreis() {return this.preis;}
	public void setPreis(double preis) {this.preis = preis;}
}
