
public class Benutzer {

	private String name;
	private int benutzerNummer;
	
	public Benutzer(String name, int benutzerNummer) {
		this.name = name;
		this.benutzerNummer = benutzerNummer;
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBenutzerNummer() {
		return this.benutzerNummer;
	}
	public void setBenutzerNummer(int benutzerNummer) {
		this.benutzerNummer = benutzerNummer;
	
	}
	public String toString() {
		return String.format("%s%-10s%-20s%-6d%-2s%n","(Name: ",this.name, ", Benutzernummer: ", this.benutzerNummer, ")");
	}
}