import java.util.ArrayList;
import java.util.Scanner;

public class Benutzerverwaltung {
	
	private static ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();
	
	public static void main(String[] args) {
		
		int auswahl;
		
		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				 benutzerAnzeigen();
				break;
			case 2:
				 bentzerErfassen();
				break;
			case 3:
				 benutzerLöschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");				
			}
		}while (true );
		
	}
	
	public static void benutzerAnzeigen() {
		for(int i=0; i < benutzerliste.size(); i++) {
			System.out.printf("%s%-10s%-20s%-6d%-2s%n","(Name: ",benutzerliste.get(i).getName(), ", Benutzernummer: ", benutzerliste.get(i).getBenutzerNummer(), ")");
					//benutzerliste.get(i) );
		}
	}
	public static void bentzerErfassen() {
		Scanner input = new Scanner(System.in);
		System.out.println("Bitte Namen der Person angeben:");
		String name = input.next();
		System.out.println("Bitte Benutzernummer der Person angeben:");
		int bn = input.nextInt();
		Benutzer b1 = new Benutzer(name, bn);
		benutzerliste.add(b1);
	}
	public static void benutzerLöschen() {
		Scanner input = new Scanner(System.in);
		System.out.println("Geben Sie die Benutzernummer des zu entfernenden Benutzers an!");
		int zuEntfernenderBenutzer = input.nextInt();
		for(int i=0; i < benutzerliste.size(); i++) {
			if(benutzerliste.get(i).getBenutzerNummer() == zuEntfernenderBenutzer) {
				benutzerliste.remove(i);
				break;
			}
		}
	}
	
	
	public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        /***************************************************/

        System.out.println("     Benutzerverwaltung     ");
        System.out.println("----------------------------\n");
        System.out.println("1 - Benutzer anzeigen");
        System.out.println("2 - Benutzer erfassen");
        System.out.println("3 - Benutzer löschen");
        System.out.println("4 - Ende");
         
        System.out.print("\nEingabe:  ");
        selection = input.nextInt();
        return selection;    
    }

}
