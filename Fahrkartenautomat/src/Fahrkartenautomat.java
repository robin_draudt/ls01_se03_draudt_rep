import java.util.Scanner;

class Fahrkartenautomat {

    public static double fahrkartenbestellungErfassen() {
        int anzahlTickets;
        double ticketPreis=0;
        String[] Bezeichnung = new String[]{
        	"Einzelfahrschein Berlin AB ",
        	"Einzelfahrschein Berlin BC",
        	"Einzelfahrschein Berlin ABC",
        	"Kurzstrecke",
        	"Tageskarte Berlin AB",
        	"Tageskarte Berlin BC",
        	"Tageskarte Berlin ABC",
        	"Kleingruppen-Tageskarte Berlin AB",
        	"Kleingruppen-Tageskarte Berlin BC",
        	"Kleingruppen-Tageskarte Berlin ABC"
        };
        double[] Preise = new double[] {
        	2.90,
        	3.30,
        	3.60,
        	1.90,
        	8.60,
        	9.00,
        	9.60,
        	23.50,
        	24.30,
        	24.90
        };
        System.out.println("Fahrkartenbestellvorgang:\n"
        					+ "=========================\n"
        					+ "\n"
        					+ "\n"
        					+ "W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\n");
        for(int i=0; i < Bezeichnung.length; i++) {
        	System.out.printf("%-35s[%5.2f EUR] (%2d)%n",Bezeichnung[i], Preise[i], i+1);
        }
        
        System.out.println("\n\nIhre Wahl: ");
        Scanner tastatur = new Scanner(System.in);
        	
        
        int auswahl = tastatur.nextInt();
        
        while (auswahl > Bezeichnung.length || auswahl < 1 ) {
        	System.out.println("  >>falsche Eingabe<<\nIhre Wahl: ");
        	auswahl = tastatur.nextInt();	
        }
        
        ticketPreis = Preise[auswahl-1];
        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
        

        return ticketPreis * anzahlTickets;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f� %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2�): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der R�ckgabebetrag in H�he von %4.2f� %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-M�nzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-M�nzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {
    	
    	boolean ende = false;
    	Scanner s = new Scanner(System.in);
    	while(ende == false) {
    		
            double zuZahlenderBetrag;
            double rueckgabebetrag;

            zuZahlenderBetrag = fahrkartenbestellungErfassen();
            rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
            fahrkartenAusgeben();
            rueckgeldAusgeben(rueckgabebetrag);

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                    + "Wir w�nschen Ihnen eine gute Fahrt.");
            System.out.print("\nWollen Sie weitere Fahrscheine erwerben? (Y/N)");
            
            String janein= s.next();
            if (janein.equals("n") || janein.equals("N")){
            	ende = true;
            }
    	}
    }
}