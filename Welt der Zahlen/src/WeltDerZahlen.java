/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstraße
    double anzahlSterne = 2E11;
    
    // Wie viele Einwohner hat Berlin?
        int bewohnerBerlin = 3800000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       short alterTage = 9672;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm= 190000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       int flaecheGroessteLand = 17100000;
    
    // Wie groß ist das kleinste Land der Erde?
    
       float flaecheKleinsteLand =  (float) 0.4;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: "+ anzahlSterne);
    System.out.println("Anzahl der Bewohner von Berlin: "+ bewohnerBerlin);
    System.out.println("Mein Alter in Tagen: "+ alterTage);
    System.out.println("Gewicht des Schwersten Tires: "+ gewichtKilogramm);
    System.out.println("Fl�che in Quadratkilometer des gr��ten Landes: "+ flaecheGroessteLand);
    System.out.println("Fl�che in Quadratkilometer des kleinsten Landes: "+ flaecheKleinsteLand);
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

